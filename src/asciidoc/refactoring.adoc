= Refactoring
:source-highlighter: highlight.js
:revealjs_theme: dracula
:revealjs_history: true
:revealjs_slideNumber: true
:revealjs_width: 1200
:revealjsdir: reveal.js
:customcss: css/custom.css


== C'est quoi ? 

* changer l'implémentation
* sans changer le comportement

[.notes]
--
* la subtilité est importante
--

[%notitle]
=== Ne pas refactorer

[quote,Anonyme]
If it ain't broke, don't fix it

=== Refactorer

* en préparation à l'ajout d'une fonctionnalité

[%step]
[quote,Kent Beck]
First make the change easy, then make the easy change

=== Refactorer

* après l'ajout d'une fonctionnalité

[%step]
[quote,Kent Beck]
Make it work, Make it right, Make it fast

=== C'est parti 
