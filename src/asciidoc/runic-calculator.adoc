= Projet : Calculatrice Runique
:source-highlighter: highlight.js
:highlightjs-theme: reveal.js/plugin/highlight/monokai.css
:revealjs_theme: dracula
:revealjs_history: true
:revealjs_slideNumber: true
:revealjs_width: 1200
:revealjsdir: reveal.js
:customcss: css/custom.css

== Présentation

La calculatrice runique est un outil permettant de travailler avec des nombres runiques : 

* traduction en nombres décimaux
* addition de nombres runiques

_Cet exercice est purement fictif et ne repose sur aucun fondement scientifique ou archéologique._

include::slides/project/_specifications__chapter_1.adoc[]
include::slides/project/_specifications__chapter_2.adoc[]
include::slides/project/_specifications__chapter_3.adoc[]
include::slides/project/_specifications__technical.adoc[]