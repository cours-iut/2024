
== Outillage

[%step]
* votre IDE
* les linters
* les analyseurs de code

=== Refactoring automatisé avec l'IDE

* renommer un token
* extraire une méthode
* "inliner" un token 

[.notes]
--
* démo !
--

=== Les linters

* éliminer les mauvaises pratiques 
* forcer les conventions

[.notes]
--
* == vs ===
--

=== Exemple de linter

* eslint
* tslint

[.notes]
--
* démo !
--

=== L'analyse de code statique

* analyse plus fine du code
* détection de duplication
* détection de problèmes de sécurité

=== Exemple d'analyseur de code 

* SonarQube

[.notes]
--
* démo !
--
