
== Mise en pratique

=== Live !

[.notes]
--
* sur theatrical-player
* sur demo-backend 
--

=== Consigne pour tous les exercices

* 🚫 Interdiction de renommer manuellement
* 🚫 Interdiction de créer des méthodes manuellement

=> utilisez l'IDE

=== Chasse aux commentaires

1. Chercher dans le projet des commentaires
2. Choisir ceux qui semble décrire le comportement du code
3. Renommer le code pour ne plus avoir besoin du commentaire 

[%notitle]
=== Exemple 

[source,javascript]
----
// si on fait une demande de modification (code 2)
// et que la priorité est normale  
if(x == 2 && priorities.include(priority)) {
    // ...
}
----

VS

[%step]
--
[source,javascript]
----
if(typeDemande == MODIFICATION && allowedPriorityCodes.include(priorityCode)) {
    // ...
}
----
--

[%step]
--
[source,javascript]
----
if(typeDemande == MODIFICATION && isPriorityCodeAllowed(priorityCode)) {
    // ...
}
----
--

[%step]
--
[source,javascript]
----
if(isAnAllowedModificationRequest(typeDemande, priorityCode)) {
    // ...
}
----
--

=== Renommer le code d'un autre 

1. Installer un linter
2. Choisir un fichier du projet que vous n'avez pas écrit
3. Appliquer la règle du boy scout

