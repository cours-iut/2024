# Print available goals
default: 
    just --list

# Build all
build_all: build_site build_slides build_code build_pdf_all


#
# Versions
#
revealjs_version := "5.0.5"
asciidoctor_image_version := "1.67"
sonarqube_image_version := "9.9.4-community"
decktape_image_version := "3.12.0"


#
# Directory structure
#
input_dir := "src/asciidoc"
input_slides_dir := input_dir / "slides"
output_dir := "dist"
assets_dir := output_dir / "assets"
css_dir := output_dir / "css"
revealjs_dl_url := "https://github.com/hakimel/reveal.js/archive/refs/tags/" + revealjs_version + ".zip"


#
# Stage : cleaning
#

# Remove the output folder
clean:
    rm -rvf {{output_dir}}
# Remove generated content
clean_assets:
    rm -rvf {{assets_dir}}


#
# Stage : before build
#

# Prepare build environment (dist folder, reveal.js, assets, ...)
prepare: build_setup revealjs_setup copy_assets

# Copy every image into the ouput directory
copy_assets: clean_assets
    mkdir -p {{assets_dir}}
    find {{input_slides_dir}} -type f -iregex '.*\.\(jpg\|jpeg\|gif\|png\)' -exec cp -v {} {{assets_dir}}/ \;
    mkdir -p {{css_dir}}
    find {{input_slides_dir}} -type f -iregex '.*\.\(css\)' -exec cp -v {} {{css_dir}}/ \;

# Download Reveal.js and place it into the output folder
revealjs_setup: build_setup
    wget {{revealjs_dl_url}}
    unzip {{revealjs_version}}.zip
    rm {{revealjs_version}}.zip
    mv reveal.js-{{revealjs_version}} {{output_dir}}/reveal.js

# Create the output directory structure
[private]
build_setup:
    mkdir -p {{output_dir}}


#
# Build : site
# 

# Generate main page from README file
build_site: build_setup (asciidoctor "README.adoc" "index.html")

# Render an AsciiDoc file into HTML
[private]
asciidoctor input_file output_file: 
    podman run \
        --rm \
        -it \
        -v ${PWD}:/app \
        -w /app \
        asciidoctor/docker-asciidoctor:{{asciidoctor_image_version}} \
        asciidoctor {{input_file}} --out-file dist/{{output_file}}


#
# Build : slides
#

# List available slides to build
list_slides: 
    ls {{input_dir}} | grep ".adoc" | sed "s/.adoc//g" 

# Generate all slides
build_slides: (build_slide "*") 

# Generate a speficic slide | Usage : just slide presentation
build_slide target: copy_assets (asciidoctor_revealjs target)

# Render an AsciiDoc file into a Reveal.js slideshow
[private]
asciidoctor_revealjs input_file: 
    podman run \
        --rm \
        -it \
        -v ${PWD}:/app \
        -w /app \
        asciidoctor/docker-asciidoctor:{{asciidoctor_image_version}} \
        asciidoctor-revealjs {{input_dir}}/{{input_file}}.adoc --destination-dir dist/


#
# Build : slides as PDF
#

# Build all PDF
build_pdf_all:
    ls {{input_dir}}/*.adoc | sed 's#{{input_dir}}/\(.*\)\.adoc#\1#g' | xargs -I{} just build_pdf {}

# Build PDF
build_pdf target: (build_slide target)
    podman run \
        --rm \
        -it \
        --userns=keep-id \
        -v ${PWD}:/app \
        astefanutti/decktape:{{decktape_image_version}} \
        /app/dist/{{target}}.html /app/dist/{{target}}.pdf

#
# Build : code
#

# Lint and test code snippets
build_code:
    cd demos/katas && yarn build
    cd demos/katas && yarn test --coverage

# Publish coverage
publish_code: build_code
    rm -rvf -- {{output_dir}}/coverage
    mkdir -pv -- {{output_dir}}/coverage 
    cp -rv -- demos/katas/coverage/lcov-report/* {{output_dir}}/coverage


#
# Run : serve the website
#

# Serve the website on localhost:3000
serve: 
    podman run \
        --rm \
        -it \
        -p 8080:80 \
        -v ${PWD}/dist:/usr/share/nginx/html:ro \
        nginx:stable


#
# Dev experience
#

# Slide livereload build mode
watch target:
    watch just build_slide {{target}}


#
# Démo
#

# Create Sonarqube server (only the first time)
sonar_create_server: 
    podman run \
        -it \
        --name sonar \
        -p 9000:9000 \
        --userns=keep-id \
        -v ./docker/sonarqube/volume/data:/opt/sonarqube/data:rw \
        sonarqube:{{sonarqube_image_version}}

# Start Sonarqube
sonar: 
    podman start sonar
    podman logs -f sonar

# Run scanner
sonar_scan:
    podman run \
        --rm \
        -it \
        --network=container:sonar \
        --userns=keep-id \
        -v ${PWD}:/usr/src:rw \
        -e SONAR_HOST_URL="http://sonar:9000" \
        -e SONAR_SCANNER_OPTS="-Dsonar.projectKey=Demo -Dsonar.javascript.lcov.reportPaths=demos/katas/coverage/lcov.info -Dsonar.sources=demos/katas/src -Dsonar.tests=demos/katas/test" \
        -e SONAR_LOGIN="sqp_b82aa77972871c62900011311065b8bd8829b8a5" \
        sonarsource/sonar-scanner-cli:5.0.1

# Start SPA demo for debug slides
demo_debugger_frontend: 
    cd demos/demo-frontend && yarn dev --port=8090

# Start backend demo for debug slides
demo_debugger_backend:
    cd demos/demo-backend && yarn start:debug
