with (import <nixpkgs> {});

mkShell {
    buildInputs = [
        yarn
        nodejs-18_x
        pandoc
        chromium
        bats
    ];

    shellHook = ''
      yarn config set prefix $PWD/.yarn
      export PATH="$(yarn global bin):$PATH"

      export PUPPETEER_PRODUCT=chrome
      export PUPPETEER_EXECUTABLE_PATH=$(which chromium)
      export PUPPETEER_SKIP_DOWNLOAD=true
    '';
}
