  import express, { Request } from 'express';
  import dotenv from 'dotenv';

  dotenv.config();

  const app = express();
  const port = process.env.PORT;

  app.listen(port, () => {
    console.log(`[server]: Server is running at http://localhost:${port}`);
  });

  app.get('/', (_, res) => {
    res.send('Express + TypeScript Server');
  });

  app.get('/hello/:name', (req: Request<{name: string}>, res) => {
    res.send(`Hello ${req.params.name}`)
  });

  app.get('/fizzbuzz/:i', (req: Request<{i: number}>, res) => {
    const i = req.params.i

    let fb;

    if(i % 15 == 0) {
      fb = 'FizzBuzz'
    }
    else if(i % 3 == 0) {
      fb = 'Fizz'
    } 
    else if(i % 5 == 0) {
      fb = 'Buzz'
    }
    else { 
      fb = `${i}`
    }

    res.send(fb)
  })