import Classe from "@/exercices/game/Classe";

class Arme {
    constructor(
        public readonly nom: string, 
        public readonly classeRequise: Classe
    ) {}
}

export default Arme;