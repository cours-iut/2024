import Classe from "@/exercices/game/Classe";
import Arme from "@/exercices/game/Arme";
import Carriable from "@/exercices/game/Carriable";
import TwitterApi from "@/exercices/game/TwitterApi";

class Joueur {
    public readonly inventaire: Carriable[]

    constructor(
        public readonly name: string, 
        public readonly classe: Classe
    ) {
        this.inventaire = [];
    }

    canEquip(arme: Arme) {
        if (this.classe === Classe.GUERRIER || this.classe === Classe.VOLEUR) {
            return arme.classeRequise === Classe.GUERRIER || arme.classeRequise === Classe.VOLEUR;
        }
        return arme.classeRequise === this.classe;
    }

    tweet() {
        new TwitterApi().sendTweet(`Rejoignez moi dans ce super RPG ! http://join.me.at.rpg/${this.name}`);
    }
}

export default Joueur;