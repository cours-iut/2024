
const SUNDAY = 0
const SATURDAY = 6

export default class WeekendChecker {

    /* 
     * 1. Tester les différents cas
     * 2. Utiliser le coverage pour vérifier le 100%
     * 3. Ajouter : "Almost !" pour le vendredi
     */

    check(): string {
        const today = new Date(Date.now())

        if(today.getDay() === SUNDAY || today.getDay() === SATURDAY) {
            return "Yes !"
        }

        return "No :("
    }
}