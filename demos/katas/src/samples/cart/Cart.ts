import Item from "@/samples/cart/Item"

export default class Cart { 

    private itemsInCart: Item[] = []

    constructor(initialItems: Item[] = []) {
        this.itemsInCart = initialItems
    }

    get items() {
        return this.itemsInCart
    }

    get total(): number {
        return this.itemsInCart.map(item => item.price).reduce((acc, i) => acc + i, 0)
    }

    isEmpty() {
        return this.itemsInCart.length == 0
    }

    addItem(item: Item) {
        this.itemsInCart.push(item)
    }

    removeItem(item: Item) {
        this.itemsInCart = this.itemsInCart.filter(elt => elt.id != item.id)
    }
}
