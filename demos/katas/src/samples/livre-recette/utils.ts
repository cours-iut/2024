import { Recette } from "./types";

export function fetchRecetteById(id: string): Recette {
   return {
        id,
        titre: "Rougail Saucisse",
        ingredients: [
            {nom: "Riz", quantite: 300, unite: "grammes"},
            {nom: "Saucisse", quantite: 2, unite: "unités"},
            {nom: "Sauce tomate", quantite: 20, unite: "centilitres"},
            {nom: "Curcuma", quantite: 1, unite: "cuillère à café"},
        ],
        etapes: [
            {tache: "Cuire les saucisses", duree: 10, unite: "minutes"},
            {tache: "Ajouter la sauce tomate", duree: 1, unite: "minutes"},
            {tache: "Ajouter le curcuma", duree: 1, unite: "minutes"},
            {tache: "Laisser mijoter", duree: 15, unite: "minutes"},
            {tache: "Faire cuire le riz",duree:  15, unite: "minutes"},
            {tache: "Servir", duree: 1, unite: "minutes"},
        ]
    }
}