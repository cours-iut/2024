export default interface Adaptable<T> {
    adapteNbPersonnes(nbPersonnes: number): T
}