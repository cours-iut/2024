import Adaptable from "@/samples/livre-recette/refactored/ddd/Adaptable"

export type UniteTemps = "minutes"

export default class Etape implements Adaptable<Etape> {
    constructor(
        public readonly tache: string,
        public readonly duree: number,
        public readonly unite: UniteTemps,
        public readonly factor: number,
    ) {}

    adapteNbPersonnes(nbPersonnes: number): Etape {
        return new Etape(
            this.tache, 
            this.duree * (nbPersonnes * this.factor),
            this.unite,
            this.factor
        )
    }
}