import Etape from "@/samples/livre-recette/refactored/ddd/Etape"
import Id from "@/samples/livre-recette/refactored/ddd/Id"
import Ingredient from "@/samples/livre-recette/refactored/ddd/Ingredient"
import Recette from "@/samples/livre-recette/refactored/ddd/Recette"

function fetchRecetteById(id: string): Recette {
    return new Recette(
        new Id(id),
        "Rougail Saucisse",
        [
            new Ingredient("Riz", 300, "grammes", 1),
            new Ingredient("Saucisse", 2, "unités", 1),
            new Ingredient("Sauce tomate", 20, "centilitres", 1),
            new Ingredient("Curcuma", 1, "cuillère à café", 0.5),
        ],
        [
            new Etape("Cuire les saucisses", 10, "minutes", 0.25),
            new Etape("Ajouter la sauce tomate", 1, "minutes", 0),
            new Etape("Ajouter le curcuma", 1, "minutes", 0),
            new Etape("Laisser mijoter", 15, "minutes", 0),
            new Etape("Faire cuire le riz", 15, "minutes", 0),
            new Etape("Servir", 1, "minutes", 0),
        ]
    )
}

export interface OptionsRecette {
    nbPersonnes: number
}

export default class RecetteController {
    getRecette(id: string, { nbPersonnes = 4 }: OptionsRecette) {
        const recette: Recette = fetchRecetteById(id)
        return recette.adapteNbPersonnes(nbPersonnes)
    }
}