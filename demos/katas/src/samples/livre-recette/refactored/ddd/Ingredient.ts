import Adaptable from "@/samples/livre-recette/refactored/ddd/Adaptable"

export type UniteIngredient = "grammes" | "unités" | "centilitres" | "cuillère à café"

export default class Ingredient implements Adaptable<Ingredient> {
    constructor(
        public readonly nom: string,
        public readonly quantite: number,
        public readonly unite: UniteIngredient,
        public readonly factor: number
    ) {}

    adapteNbPersonnes(nbPersonnes: number): Ingredient {
        return new Ingredient(
            this.nom, 
            this.quantite * (nbPersonnes * this.factor),
            this.unite,
            this.factor
        )
    }
}