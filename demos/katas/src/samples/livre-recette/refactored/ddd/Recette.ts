import Adaptable from "@/samples/livre-recette/refactored/ddd/Adaptable"
import Etape from "@/samples/livre-recette/refactored/ddd/Etape"
import Id from "@/samples/livre-recette/refactored/ddd/Id"
import Ingredient from "@/samples/livre-recette/refactored/ddd/Ingredient"

export default class Recette implements Adaptable<Recette> {
    constructor(
        public readonly id: Id,
        public readonly titre: string,    
        public readonly ingredients: Ingredient[],
        public readonly etapes: Etape[],
    ) {}

    adapteNbPersonnes(nbPersonnes: number) {
        return new Recette(
            this.id,
            this.titre,
            this.ingredients.map(ingredient => ingredient.adapteNbPersonnes(nbPersonnes)),
            this.etapes.map(etape => etape.adapteNbPersonnes(nbPersonnes)),
        )
    }
}


