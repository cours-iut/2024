import { Recette } from "@/samples/livre-recette/types"
import { fetchRecetteById } from "@/samples/livre-recette/utils"

export interface OptionsRecette {
   nbPersonnes: number 
}

export default class RecetteController {
    getRecette(idRecette: string, { nbPersonnes = 4 }: OptionsRecette): Recette {
        const {id, titre, ingredients, etapes} = fetchRecetteById(idRecette)
        return { 
            id, 
            titre,
            ingredients: ingredients.map(ingredient => ({...ingredient, quantite: ingredient.quantite * nbPersonnes})),
            etapes: etapes.map(etape => ({...etape, time: etape.duree * (nbPersonnes / 2)})),
        }
    }
}
