export interface Recette {
    id: string,
    titre: string,
    ingredients: Array<{nom: string, quantite: number, unite: string}>
    etapes: Array<{tache: string, duree: number, unite: string}>
}

