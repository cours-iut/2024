import { RandomGenerator } from "./RandomGenerator";

class Dice {
  constructor(
    private random: RandomGenerator
  ) {}

  roll() {
    return this.random.nextInt();
  }
}

export function withFaces(n: number): Dice {
  return new Dice({
    nextInt() {
      return Math.floor(Math.random() * (n)) + 1;
    },
  });
}

export default Dice;
