
export interface RandomGenerator {
  nextInt(): number;
}
