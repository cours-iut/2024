/* eslint-disable @typescript-eslint/no-unused-vars, @typescript-eslint/no-explicit-any */

export class ImaginaryDBConnection {
    constructor(
        private url: string,
        private port: number,
        private user: string,
        private password: string,
    ) {
        // nothing to do here
    }


    query<T>(sql: string): ImaginaryEntities {
        return new ImaginaryEntities([
            {
                title: "Lorem Ipsum",
                status: "PUBLISHED",
                content: "",
                author: "jdoe",
            },

        ])
    }
}

export class ImaginaryEntities {

    constructor(
        public rawContent: any[]
    ) {
        // nothing to do here
    }

    as<T>(converter: ImaginaryConverter<T>): Array<T> {
        return this.rawContent.map(it => converter(it))
    }
}

export type ImaginaryConverter<T> = (entity: any) => T 