export type ArticleStatus = "DRAFT" | "IN_REVIEW" | "PUBLISHED"

export default class Article {
    constructor(
        public title: string,
        public content: string,
        public author: string,
        public status: ArticleStatus,
    ) {
        // nothing to do here
    }
}