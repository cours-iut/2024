import Article from "@/samples/blog/Article";
import { ImaginaryDBConnection } from "@/samples/blog/libs/imaginary-db";
import { readFileSync } from "fs";

export default class ArticleService {

    private dbConnection: ImaginaryDBConnection

    constructor() {
        const configurationRawContent = readFileSync('./src/samples/blog/configuration.json')
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        const configuration: { dbUrl: string, dbPort: number, dbUser: string, dbPassword: string } 
            = JSON.parse(configurationRawContent.toString())

        this.dbConnection = new ImaginaryDBConnection(
            configuration.dbUrl,
            configuration.dbPort,
            configuration.dbUser,
            configuration.dbPassword
        )
    }

    getArticles(): Article[] {
        return this.dbConnection.query("SELECT * FROM articles")
            .as<Article>(({ title, content, author, status }: Article) => {
                return new Article(title, content, author, status)
            })
            .filter(article => article.status == "PUBLISHED")
    }
}


