import Article from "@/samples/blog/Article";
import { ImaginaryDBConnection } from "@/samples/blog/libs/imaginary-db";
import { readFileSync } from "fs";

export default class ArticleService {

    constructor(
        private dao: ArticleDAO
    ) {
        // nothing to do here
    }

    getArticles(): Article[] {
      return this.dao.getArticles() 
            .filter(article => article.status == "PUBLISHED")
    }
}

export class ArticleDAO {

    private dbConnection: ImaginaryDBConnection

    constructor(configuration: { dbUrl: string, dbPort: number, dbUser: string, dbPassword: string }) {
        this.dbConnection = new ImaginaryDBConnection(
            configuration.dbUrl,
            configuration.dbPort,
            configuration.dbUser,
            configuration.dbPassword
        )
    }

    getArticles(): Article[] {
        return this.dbConnection.query("SELECT * FROM articles")
            .as<Article>(({ title, content, author, status }: Article) => {
                return new Article(title, content, author, status)
            })
            
    }
}

export class ConfigurationManager {

    public readonly configuration: { dbUrl: string, dbPort: number, dbUser: string, dbPassword: string };  

    constructor() {
        const configurationRawContent = readFileSync('./src/samples/blog/configuration.json')
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        this.configuration = JSON.parse(configurationRawContent.toString())
    }
}
