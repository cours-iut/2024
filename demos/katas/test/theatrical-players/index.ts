/* eslint-disable @typescript-eslint/no-unsafe-argument, @typescript-eslint/no-explicit-any, @typescript-eslint/no-unsafe-assignment */

import { statement } from "@/theatrical-players/index";
import fs from "fs";

function getResource(path: string): any {
    return JSON.parse(
        fs.readFileSync(
            `test/theatrical-players/${path}`,
            "utf-8"
        )
    )
}

test("example statement", () => {
    const invoice = getResource("invoice.json");
    const plays = getResource("plays.json");
    expect(statement(invoice, plays)).toMatchSnapshot();
});

test("statement with new play types", () => {
    const invoice = getResource("invoice_new_plays.json")
    const plays = getResource("new_plays.json")
    expect(() => { statement(invoice, plays); }).toThrow(/unknown type/);
});