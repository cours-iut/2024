/* eslint-disable @typescript-eslint/no-unsafe-argument, @typescript-eslint/no-explicit-any, @typescript-eslint/no-unsafe-assignment */

import { statement } from "@/video-store";
import fs from "fs";

function getResource(path: string): any {
    return JSON.parse(
        fs.readFileSync(
            `test/video-store/${path}`,
            "utf-8"
        )
    )
}

test("example statement", () => {
    const customer = getResource("customer.json");
    const movies = getResource("movies.json");
    expect(statement(customer, movies)).toMatchSnapshot();
});
