import RecetteController from "@/samples/livre-recette/RecetteController"
import { Recette } from "@/samples/livre-recette/types"

test('should return a recipe', () => {
    // Arrange
    const controller = new RecetteController()

    // Act
    const recipe: Recette = controller.getRecette("foo", { nbPersonnes: 2 })

    // Assert
    expect(recipe.titre).toEqual("Rougail Saucisse")
})

test('should adapt ingredients quantity', () => {
    // Arrange
    const controller = new RecetteController()

    // Act
    const recipe: Recette = controller.getRecette("foo", { nbPersonnes: 2 })

    // Assert
    const rice = recipe.ingredients.find(ingredient => ingredient.nom == "Riz")
    expect(rice).not.toBeNull()
    expect(rice?.quantite).toEqual(600)
})