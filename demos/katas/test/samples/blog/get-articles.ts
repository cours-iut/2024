import Article from "@/samples/blog/Article"
import ArticleService from "@/samples/blog/ArticleService"
import ArticleServiceDI, { ArticleDAO } from "@/samples/blog/refactored/di/ArticleService"
import ArticleServiceSrp from "@/samples/blog/refactored/srp/ArticleService"

it("should get articles from imaginary db", () => {
    // Arrange
    const articleService = new ArticleService()

    // Act
    const articles = articleService.getArticles()

    // Assert
    expect(articles.length).toBeGreaterThan(0)
})

it("should still work after SRP refactoring", () => {
    // Arrange
    const articleService = new ArticleServiceSrp()

    // Act
    const articles = articleService.getArticles()

    // Assert
    expect(articles.length).toBeGreaterThan(0)
})

it("should still work after DI refactoring", () => {
    // Arrange
    const articleDao = new ArticleDAO({
        dbUrl: "imaginarydb",
        dbPort: 1111,
        dbUser: "test",
        dbPassword: "password"
    })

    const articleService = new ArticleServiceDI(articleDao)

    // Act
    const articles = articleService.getArticles()

    // Assert
    expect(articles.length).toBeGreaterThan(0)
})

it("should still work with an alternative dao implementation", () => {
    // Arrange
    const articleDao = new ArticleDAO({ // new InMemoryArticleDAO({
        dbUrl: "imaginarydb",
        dbPort: 1111,
        dbUser: "test",
        dbPassword: "password"
    })

    const articleService = new ArticleServiceDI(articleDao)

    // Act
    const articles = articleService.getArticles()

    // Assert
    expect(articles.length).toBeGreaterThan(0)
})

class InMemoryArticleDAO extends ArticleDAO {
    getArticles(): Article[] {
        return [
            new Article(
                "Lorem ipsum",
                "Some content",
                "Me",
                "PUBLISHED"
            )
        ]
    }
}