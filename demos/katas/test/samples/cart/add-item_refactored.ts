import Cart from "@/samples/cart/Cart";
import Item from "@/samples/cart/Item";

// tag::before_each[]
let cart: Cart; 

beforeEach(() => {
    cart = new Cart()
})
// end::before_each[]

// tag::test_before_each[]
test("an item can be added into a cart", () => {
   // Arrange

   // Act
   cart.addItem(new Item(1, "Keyboard", 100))

   // Assert
   expect(cart.items).toHaveLength(1);
})
// end::test_before_each[]

test("multiple items can be added into a cart", () => {
    // Arrange

    // Act
    cart.addItem(new Item(1, "Keyboard", 100))
    cart.addItem(new Item(2, "Mouse", 50))

    // Assert
    expect(cart.items).toHaveLength(2);
})

// tag::factory[]
function createEmptyCart() {
    return new Cart()
}
// end::factory[]

// tag::test_factory[]
test("multiple items can be added into a cart", () => {
    // Arrange
    const emptyCart = createEmptyCart() 

    // Act
    emptyCart.addItem(new Item(1, "Keyboard", 100))
    emptyCart.addItem(new Item(2, "Mouse", 50))

    // Assert
    expect(emptyCart.items).toHaveLength(2)
})
// end::test_factory[]