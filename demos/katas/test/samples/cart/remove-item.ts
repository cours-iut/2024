import Cart from "@/samples/cart/Cart"
import Item from "@/samples/cart/Item"

test("an item can be removed", () => {
    // Arrange
    const cart = new Cart([
        new Item(1, "Keyboard", 100),
        new Item(2, "Mouse", 100),
    ])

    // Act
    cart.removeItem(
        new Item(1, "Keyboard", 100)
    )

    // Assert
    expect(cart.items).toHaveLength(1)
})