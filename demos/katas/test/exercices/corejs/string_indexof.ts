/*
 * Tester les différents cas possible pour la fonction "indexOf"
 *
 * Modèle de test :

test('description du test', () => {
    // Arrange
    const helloWorld = "Hello World !"
    
    // Act
    const index = helloWorld.indexOf("__TODO__")

    // Assert
    expect(index).toEqual("__TODO__")
})
*/

test('description du test', () => {
    // Arrange
    const helloWorld = "Hello World !"
    
    // Act
    const index = helloWorld.indexOf("__TODO__")

    // Assert
    // expect(index).toEqual("__TODO__")
})
