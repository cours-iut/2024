
/*
 * Tester les usages suivants de Array.splice()
 * 
 * 1. Supprimer "Cat"
 * 2. Ajouter un élément, ie "Lizard"
 * 3. Remplacer "Lion" par "Lizard"
 * 
 * Modèle : 

test("description du test", () => {
    // Arrange
    const animals = ["Cat", "Dog", "Bird", "Lion", "Elephant", "Ant"]

    // Act
    const removedItems = animals.splice("__TODO__");

    // Assert
    expect(removedItems).toEqual(["__TODO__"])
    expect(animals).not.toEqual(expect.arrayContaining(["__TODO__"]))
})

*/
test("description du test", () => {

    // Arrange
    const animals = ["Cat", "Dog", "Bird", "Lion", "Elephant", "Ant"]

    // Act
    // const removedItems = animals.splice(__TODO__);

    // Assert
    // expect(removedItems).toEqual(["__TODO__"])
    // expect(animals).not.toEqual(expect.arrayContaining(["__TODO__"]))
})

