import './assets/main.css'
import 'todomvc-app-css/index.css';
import 'todomvc-common/base.css'

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

const app = createApp(App)

app.use(router)

app.mount('.todoapp')
